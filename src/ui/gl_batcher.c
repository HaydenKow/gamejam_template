// gl_batcher.c: handles creating little batches of polys

#include "gl_batcher.h"

// these could go in a vertexbuffer/indexbuffer pair
#define MAX_BATCHED_SURFVERTEXES 512
#define MAX_BATCHED_SURFINDEXES 1028 //24288
#define VERTEXSIZE_SINGLE 5
#define VERTEXSIZE_COLOR 8
const int sizeofvertex = VERTEXSIZE_SINGLE * sizeof(float);
const int sizeofvertexcolor = VERTEXSIZE_COLOR * sizeof(float);

static float r_batchedsurfvertexes[MAX_BATCHED_SURFVERTEXES * VERTEXSIZE_SINGLE];
static unsigned short r_batchedsurfindexes[MAX_BATCHED_SURFINDEXES];

static int r_numsurfvertexes = 0;
static int r_numsurfindexes = 0;

static bool multi = false;
static bool is_color = false;
static vec3 _color;

void R_BeginBatchingSurfaces(int texcoordindex)
{
   //glEnableClientState(GL_VERTEX_ARRAY);
   glVertexPointer(3, GL_FLOAT, sizeofvertex, &r_batchedsurfvertexes[0]);

   //glEnableClientState(GL_TEXTURE_COORD_ARRAY);
   glTexCoordPointer(2, GL_FLOAT, sizeofvertex, &r_batchedsurfvertexes[texcoordindex]);

   r_numsurfvertexes = 0;
   r_numsurfindexes = 0;

   multi = false;
}

#ifdef PSP
#define R_BeginBatchingSurfacesMulti()
#else

#ifdef __MINGW32__
#define glClientActiveTextureARB glClientActiveTexture
#endif

void R_BeginBatchingSurfacesMulti()
{
   //glEnableClientState(GL_VERTEX_ARRAY);
   glVertexPointer(3, GL_FLOAT, sizeofvertex, &r_batchedsurfvertexes[0]);

   glClientActiveTextureARB(GL_TEXTURE0);
   glEnableClientState(GL_TEXTURE_COORD_ARRAY);
   glTexCoordPointer(2, GL_FLOAT, sizeofvertex, &r_batchedsurfvertexes[3]);

   glClientActiveTextureARB(GL_TEXTURE1);
   glEnableClientState(GL_TEXTURE_COORD_ARRAY);
   glTexCoordPointer(2, GL_FLOAT, sizeofvertex, &r_batchedsurfvertexes[5]);

   r_numsurfvertexes = 0;
   r_numsurfindexes = 0;

   multi = true;
}
#endif

void R_BeginBatchingSurfacesQuad()
{
   glEnable(GL_BLEND);
   glEnableClientState(GL_VERTEX_ARRAY);
   glVertexPointer(3, GL_FLOAT, sizeofvertex, &r_batchedsurfvertexes[0]);

   glEnableClientState(GL_TEXTURE_COORD_ARRAY);
   glTexCoordPointer(2, GL_FLOAT, sizeofvertex, &r_batchedsurfvertexes[3]);

   glDisable(GL_BLEND);
   glDisableClientState(GL_COLOR_ARRAY);

   r_numsurfvertexes = 0;
   r_numsurfindexes = 0;
   glm_vec3_copy(GLM_VEC3_ZERO, _color);
   is_color = false;
}

void R_BeginBatchingSurfacesQuadWithColor(vec3 color)
{
   glEnable(GL_BLEND);
   glEnableClientState(GL_VERTEX_ARRAY);
   glVertexPointer(3, GL_FLOAT, sizeofvertexcolor, &r_batchedsurfvertexes[0]);

   glEnableClientState(GL_TEXTURE_COORD_ARRAY);
   glTexCoordPointer(2, GL_FLOAT, sizeofvertexcolor, &r_batchedsurfvertexes[3]);

   glEnableClientState(GL_COLOR_ARRAY);
   glColorPointer(3, GL_FLOAT, sizeofvertexcolor, &r_batchedsurfvertexes[5]);

   r_numsurfvertexes = 0;
   r_numsurfindexes = 0;
   glm_vec3_copy(color, _color);
   is_color = true;
}

void R_EndBatchingSurfaces(void)
{
   if (r_numsurfvertexes && r_numsurfindexes)
   {
      glDrawElements(GL_TRIANGLES, r_numsurfindexes, GL_UNSIGNED_SHORT, r_batchedsurfindexes);
   }

   r_numsurfvertexes = 0;
   r_numsurfindexes = 0;
#ifdef _arch_dreamcast
   sq_clr(r_batchedsurfindexes, sizeof(r_batchedsurfindexes));
   sq_clr(r_batchedsurfvertexes, sizeof(r_batchedsurfvertexes));
#else
   memset(r_batchedsurfindexes, 0x0, sizeof(r_batchedsurfindexes));
   memset(r_batchedsurfvertexes, 0x0, sizeof(r_batchedsurfvertexes));
#endif

   multi = false;
}

void R_EndBatchingSurfacesQuads(void)
{
   if (r_numsurfvertexes && r_numsurfindexes)
   {
      glDrawArrays(GL_TRIANGLES, 0, r_numsurfindexes);
   }
   glDisable(GL_BLEND);
   glDisableClientState(GL_COLOR_ARRAY);

   r_numsurfvertexes = 0;
   r_numsurfindexes = 0;
#ifdef _arch_dreamcast
   sq_clr(r_batchedsurfindexes, sizeof(r_batchedsurfindexes));
   sq_clr(r_batchedsurfvertexes, sizeof(r_batchedsurfvertexes));
#else
   memset(r_batchedsurfindexes, 0x0, sizeof(r_batchedsurfindexes));
   memset(r_batchedsurfvertexes, 0x0, sizeof(r_batchedsurfvertexes));
#endif

   multi = false;
}

int text_size = 16;

void R_BatchSurfaceQuadText(int x, int y, float frow, float fcol, float size)
{
   if (r_numsurfvertexes + 4 >= MAX_BATCHED_SURFVERTEXES)
      R_EndBatchingSurfacesQuads();
   if (r_numsurfindexes + 4 >= MAX_BATCHED_SURFINDEXES)
      R_EndBatchingSurfacesQuads();

   if (is_color)
   {
      //Vertex 1
      //Quad vertex
      r_batchedsurfvertexes[((r_numsurfvertexes + 0) * VERTEXSIZE_COLOR) + 0] = x;
      r_batchedsurfvertexes[((r_numsurfvertexes + 0) * VERTEXSIZE_COLOR) + 1] = y;
      r_batchedsurfvertexes[((r_numsurfvertexes + 0) * VERTEXSIZE_COLOR) + 2] = 0;
      //Tex Coord
      r_batchedsurfvertexes[((r_numsurfvertexes + 0) * VERTEXSIZE_COLOR) + 3] = fcol;
      r_batchedsurfvertexes[((r_numsurfvertexes + 0) * VERTEXSIZE_COLOR) + 4] = frow;
      //Color vertex
      r_batchedsurfvertexes[((r_numsurfvertexes + 0) * VERTEXSIZE_COLOR) + 5] = _color[0];
      r_batchedsurfvertexes[((r_numsurfvertexes + 0) * VERTEXSIZE_COLOR) + 6] = _color[1];
      r_batchedsurfvertexes[((r_numsurfvertexes + 0) * VERTEXSIZE_COLOR) + 7] = _color[2];

      //Vertex 2
      //Quad vertex
      r_batchedsurfvertexes[((r_numsurfvertexes + 1) * VERTEXSIZE_COLOR) + 0] = x + text_size;
      r_batchedsurfvertexes[((r_numsurfvertexes + 1) * VERTEXSIZE_COLOR) + 1] = y;
      r_batchedsurfvertexes[((r_numsurfvertexes + 1) * VERTEXSIZE_COLOR) + 2] = 0;
      //Tex Coord
      r_batchedsurfvertexes[((r_numsurfvertexes + 1) * VERTEXSIZE_COLOR) + 3] = fcol + size;
      r_batchedsurfvertexes[((r_numsurfvertexes + 1) * VERTEXSIZE_COLOR) + 4] = frow;
      //Color vertex
      r_batchedsurfvertexes[((r_numsurfvertexes + 1) * VERTEXSIZE_COLOR) + 5] = _color[0];
      r_batchedsurfvertexes[((r_numsurfvertexes + 1) * VERTEXSIZE_COLOR) + 6] = _color[1];
      r_batchedsurfvertexes[((r_numsurfvertexes + 1) * VERTEXSIZE_COLOR) + 7] = _color[2];

      //Vertex 4
      //Quad vertex
      r_batchedsurfvertexes[((r_numsurfvertexes + 2) * VERTEXSIZE_COLOR) + 0] = x;
      r_batchedsurfvertexes[((r_numsurfvertexes + 2) * VERTEXSIZE_COLOR) + 1] = y + text_size;
      r_batchedsurfvertexes[((r_numsurfvertexes + 2) * VERTEXSIZE_COLOR) + 2] = 0;
      //Tex Coord
      r_batchedsurfvertexes[((r_numsurfvertexes + 2) * VERTEXSIZE_COLOR) + 3] = fcol;
      r_batchedsurfvertexes[((r_numsurfvertexes + 2) * VERTEXSIZE_COLOR) + 4] = frow + size;
      //Color vertex
      r_batchedsurfvertexes[((r_numsurfvertexes + 2) * VERTEXSIZE_COLOR) + 5] = _color[0];
      r_batchedsurfvertexes[((r_numsurfvertexes + 2) * VERTEXSIZE_COLOR) + 6] = _color[1];
      r_batchedsurfvertexes[((r_numsurfvertexes + 2) * VERTEXSIZE_COLOR) + 7] = _color[2];

      //Vertex 4
      //Quad vertex
      r_batchedsurfvertexes[((r_numsurfvertexes + 3) * VERTEXSIZE_COLOR) + 0] = x;
      r_batchedsurfvertexes[((r_numsurfvertexes + 3) * VERTEXSIZE_COLOR) + 1] = y + text_size;
      r_batchedsurfvertexes[((r_numsurfvertexes + 3) * VERTEXSIZE_COLOR) + 2] = 0;
      //Tex Coord
      r_batchedsurfvertexes[((r_numsurfvertexes + 3) * VERTEXSIZE_COLOR) + 3] = fcol;
      r_batchedsurfvertexes[((r_numsurfvertexes + 3) * VERTEXSIZE_COLOR) + 4] = frow + size;
      //Color vertex
      r_batchedsurfvertexes[((r_numsurfvertexes + 3) * VERTEXSIZE_COLOR) + 5] = _color[0];
      r_batchedsurfvertexes[((r_numsurfvertexes + 3) * VERTEXSIZE_COLOR) + 6] = _color[1];
      r_batchedsurfvertexes[((r_numsurfvertexes + 3) * VERTEXSIZE_COLOR) + 7] = _color[2];

      //Vertex 2
      //Quad vertex
      r_batchedsurfvertexes[((r_numsurfvertexes + 4) * VERTEXSIZE_COLOR) + 0] = x + text_size;
      r_batchedsurfvertexes[((r_numsurfvertexes + 4) * VERTEXSIZE_COLOR) + 1] = y;
      r_batchedsurfvertexes[((r_numsurfvertexes + 4) * VERTEXSIZE_COLOR) + 2] = 0;
      //Tex Coord
      r_batchedsurfvertexes[((r_numsurfvertexes + 4) * VERTEXSIZE_COLOR) + 3] = fcol + size;
      r_batchedsurfvertexes[((r_numsurfvertexes + 4) * VERTEXSIZE_COLOR) + 4] = frow;
      //Color vertex
      r_batchedsurfvertexes[((r_numsurfvertexes + 4) * VERTEXSIZE_COLOR) + 5] = _color[0];
      r_batchedsurfvertexes[((r_numsurfvertexes + 4) * VERTEXSIZE_COLOR) + 6] = _color[1];
      r_batchedsurfvertexes[((r_numsurfvertexes + 4) * VERTEXSIZE_COLOR) + 7] = _color[2];

      //Vertex 3
      //Quad vertex
      r_batchedsurfvertexes[((r_numsurfvertexes + 5) * VERTEXSIZE_COLOR) + 0] = x + text_size;
      r_batchedsurfvertexes[((r_numsurfvertexes + 5) * VERTEXSIZE_COLOR) + 1] = y + text_size;
      r_batchedsurfvertexes[((r_numsurfvertexes + 5) * VERTEXSIZE_COLOR) + 2] = 0;
      //Tex Coord
      r_batchedsurfvertexes[((r_numsurfvertexes + 5) * VERTEXSIZE_COLOR) + 3] = fcol + size;
      r_batchedsurfvertexes[((r_numsurfvertexes + 5) * VERTEXSIZE_COLOR) + 4] = frow + size;
      //Color vertex
      r_batchedsurfvertexes[((r_numsurfvertexes + 5) * VERTEXSIZE_COLOR) + 5] = _color[0];
      r_batchedsurfvertexes[((r_numsurfvertexes + 5) * VERTEXSIZE_COLOR) + 6] = _color[1];
      r_batchedsurfvertexes[((r_numsurfvertexes + 5) * VERTEXSIZE_COLOR) + 7] = _color[2];
   }
   else
   {
      //Vertex 1
      //Quad vertex
      r_batchedsurfvertexes[((r_numsurfvertexes + 0) * VERTEXSIZE_SINGLE) + 0] = x;
      r_batchedsurfvertexes[((r_numsurfvertexes + 0) * VERTEXSIZE_SINGLE) + 1] = y;
      r_batchedsurfvertexes[((r_numsurfvertexes + 0) * VERTEXSIZE_SINGLE) + 2] = 0;
      //Tex Coord
      r_batchedsurfvertexes[((r_numsurfvertexes + 0) * VERTEXSIZE_SINGLE) + 3] = fcol;
      r_batchedsurfvertexes[((r_numsurfvertexes + 0) * VERTEXSIZE_SINGLE) + 4] = frow;

      //Vertex 2
      //Quad vertex
      r_batchedsurfvertexes[((r_numsurfvertexes + 1) * VERTEXSIZE_SINGLE) + 0] = x + text_size;
      r_batchedsurfvertexes[((r_numsurfvertexes + 1) * VERTEXSIZE_SINGLE) + 1] = y;
      r_batchedsurfvertexes[((r_numsurfvertexes + 1) * VERTEXSIZE_SINGLE) + 2] = 0;
      //Tex Coord
      r_batchedsurfvertexes[((r_numsurfvertexes + 1) * VERTEXSIZE_SINGLE) + 3] = fcol + size;
      r_batchedsurfvertexes[((r_numsurfvertexes + 1) * VERTEXSIZE_SINGLE) + 4] = frow;

      //Vertex 4
      //Quad vertex
      r_batchedsurfvertexes[((r_numsurfvertexes + 2) * VERTEXSIZE_SINGLE) + 0] = x;
      r_batchedsurfvertexes[((r_numsurfvertexes + 2) * VERTEXSIZE_SINGLE) + 1] = y + text_size;
      r_batchedsurfvertexes[((r_numsurfvertexes + 2) * VERTEXSIZE_SINGLE) + 2] = 0;
      //Tex Coord
      r_batchedsurfvertexes[((r_numsurfvertexes + 2) * VERTEXSIZE_SINGLE) + 3] = fcol;
      r_batchedsurfvertexes[((r_numsurfvertexes + 2) * VERTEXSIZE_SINGLE) + 4] = frow + size;

      //Vertex 4
      //Quad vertex
      r_batchedsurfvertexes[((r_numsurfvertexes + 3) * VERTEXSIZE_SINGLE) + 0] = x;
      r_batchedsurfvertexes[((r_numsurfvertexes + 3) * VERTEXSIZE_SINGLE) + 1] = y + text_size;
      r_batchedsurfvertexes[((r_numsurfvertexes + 3) * VERTEXSIZE_SINGLE) + 2] = 0;
      //Tex Coord
      r_batchedsurfvertexes[((r_numsurfvertexes + 3) * VERTEXSIZE_SINGLE) + 3] = fcol;
      r_batchedsurfvertexes[((r_numsurfvertexes + 3) * VERTEXSIZE_SINGLE) + 4] = frow + size;

      //Vertex 2
      //Quad vertex
      r_batchedsurfvertexes[((r_numsurfvertexes + 4) * VERTEXSIZE_SINGLE) + 0] = x + text_size;
      r_batchedsurfvertexes[((r_numsurfvertexes + 4) * VERTEXSIZE_SINGLE) + 1] = y;
      r_batchedsurfvertexes[((r_numsurfvertexes + 4) * VERTEXSIZE_SINGLE) + 2] = 0;
      //Tex Coord
      r_batchedsurfvertexes[((r_numsurfvertexes + 4) * VERTEXSIZE_SINGLE) + 3] = fcol + size;
      r_batchedsurfvertexes[((r_numsurfvertexes + 4) * VERTEXSIZE_SINGLE) + 4] = frow;

      //Vertex 3
      //Quad vertex
      r_batchedsurfvertexes[((r_numsurfvertexes + 5) * VERTEXSIZE_SINGLE) + 0] = x + text_size;
      r_batchedsurfvertexes[((r_numsurfvertexes + 5) * VERTEXSIZE_SINGLE) + 1] = y + text_size;
      r_batchedsurfvertexes[((r_numsurfvertexes + 5) * VERTEXSIZE_SINGLE) + 2] = 0;
      //Tex Coord
      r_batchedsurfvertexes[((r_numsurfvertexes + 5) * VERTEXSIZE_SINGLE) + 3] = fcol + size;
      r_batchedsurfvertexes[((r_numsurfvertexes + 5) * VERTEXSIZE_SINGLE) + 4] = frow + size;
   }

   r_numsurfvertexes += 6;
   r_numsurfindexes += 6;
}
