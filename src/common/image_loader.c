/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common\image_loader.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common
 * Created Date: Saturday, June 29th 2019, 9:14:19 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */
#include "image_loader.h"

#define STB_IMAGE_IMPLEMENTATION
#define STBI_NO_FAILURE_STRINGS

#if 0
#define STBI_NO_BMP
#define STBI_FAILURE_USERMSG
#endif
#define STBI_NO_PSD
#define STBI_NO_TGA
#define STBI_NO_GIF
#define STBI_NO_HDR
#define STBI_NO_PIC
#define STBI_NO_PNM

#include <stb_image.h>

image IMG_load(char *path)
{
    image img;
    stbi_set_flip_vertically_on_load(false); // maybe true?
    img.data = stbi_load(transform_path(path),
                                     &img.width,
                                     &img.height,
                                     &img.channels,
                                     STBI_rgb_alpha); //maybe should be rgb
    const char *stbi_reason = stbi_failure_reason();
    if(stbi_reason != NULL)
        printf("ERROR: for path(%s), %s\n",transform_path(path),stbi_reason);

    return img;
}

void IMG_unload(image *img){
    stbi_image_free(img->data);
}
