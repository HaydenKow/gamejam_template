#include "common.h"
#include "renderer.h"
#include "input.h"
#include "image_loader.h"
#include "ui/ui_backend.h"

GLubyte indices[] = {0, 1, 2, 2, 3, 0,        // front
                     4, 5, 6, 6, 7, 4,        // right
                     8, 9, 10, 10, 11, 8,     // top
                     12, 13, 14, 14, 15, 12,  // left
                     16, 17, 18, 18, 19, 16,  // bottom
                     20, 21, 22, 22, 23, 20}; // back

GLfloat vertices3[] = {1, 1, 1, 1, 1, 1, // v0 (front)
                       0, 1, 1, 1, 1, 0, // v1
                       0, 0, 1, 1, 0, 0, // v2
                       1, 0, 1, 1, 0, 1, // v3

                       1, 1, 1, 1, 1, 1, // v0 (right)
                       1, 0, 1, 1, 0, 1, // v3
                       1, 0, 0, 0, 0, 1, // v4
                       1, 1, 0, 0, 1, 1, // v5

                       1, 1, 1, 1, 1, 1, // v0 (top)
                       1, 1, 0, 0, 1, 1, // v5
                       0, 1, 0, 0, 1, 0, // v6
                       0, 1, 1, 1, 1, 0, // v1

                       0, 1, 1, 1, 1, 0, // v1 (left)
                       0, 1, 0, 0, 1, 0, // v6
                       0, 0, 0, 0, 0, 0, // v7
                       0, 0, 1, 1, 0, 0, // v2

                       0, 0, 0, 0, 0, 0, // v7 (bottom)
                       1, 0, 0, 0, 0, 1, // v4
                       1, 0, 1, 1, 0, 1, // v3
                       0, 0, 1, 1, 0, 0, // v2

                       1, 0, 0, 0, 0, 1,  // v4 (back)
                       0, 0, 0, 0, 0, 0,  // v7
                       0, 1, 0, 0, 1, 0,  // v6
                       1, 1, 0, 0, 1, 1}; // v5

image atlas;
void LoadGLTextures(void);

void Game_Main(__attribute__((unused)) int argc, __attribute__((unused)) char **argv)
{
    InitGL(SCR_WIDTH, SCR_HEIGHT);
    UI_Init();
    LoadGLTextures();
}

/* floats for x rotation, y rotation, z rotation */
float xrot, yrot, zrot;

// Load Bitmaps And Convert To Textures
void LoadGLTextures()
{
    atlas = IMG_load("tiles.png");
    RNDR_CreateTextureFromImage(&atlas);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, atlas.id);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
}

int visible = 0;
float angle = 0.0f;
float radius = 3.0f;

void Host_Input(__attribute__((unused)) float time)
{
}

void Host_Frame(float time)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear The Screen And The Depth Buffer
    glLoadIdentity();                                   // Reset The View
    angle = Sys_FloatTime();
   
    float camX = sin(angle) * radius;
    float camZ = cos(angle) * radius;

    glDisable(GL_TEXTURE_2D);
    InitGL(SCR_WIDTH, SCR_HEIGHT);
    glPushMatrix();
    gluLookAt(camX, radius, camZ, 0.5f,0.5f,0.5f, 0.0f, 1.0f, 0.0f);
    glEnableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
    glDisable(GL_TEXTURE_2D);
    glColorPointer(3, GL_FLOAT, 6 * sizeof(GLfloat), vertices3 + 3);
    glVertexPointer(3, GL_FLOAT, 6 * sizeof(GLfloat), vertices3);
    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_BYTE, indices);
    glPopMatrix();
    
    UI_Set2D();
    char mspf[32];
    sprintf(mspf, "MSPF: %.02f FPS: %d", (double)time * 1000, (int)((1000 / time) / 1000));
    UI_TextColor(1.0f,1.0f,1.0f);
    UI_DrawString(80, 80, "TUVWU");
    UI_TextColor(1.0f,0,0);
    UI_DrawString(80, 120, mspf);
}