#ifndef COMMON_H
#define COMMON_H
/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common\common.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common
 * Created Date: Saturday, June 29th 2019, 6:28:41 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */
#ifdef _arch_dreamcast
#include <kos.h>
#include <dc/fmath.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#include "file_access.h"
#include "types.h"

extern void Game_Main(int argc, char **argv);
extern void Host_SetupChunk(void);

//Per system functions
extern double Sys_FloatTime(void);

extern void Game_InputHandler(char c);
extern void Host_Input(float time);

#endif /* COMMON_H */
