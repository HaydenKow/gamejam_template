#ifndef INPUT_H
#define INPUT_H
/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common\input.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common
 * Created Date: Saturday, July 6th 2019, 6:23:18 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */
#include "common.h"

typedef enum
{
    BTN_NULL,
    BTN_A,
    BTN_B,
    BTN_X,
    BTN_Y,
    BTN_START
} BUTTON;
typedef enum
{
    ACTION_NULL,
    BTN_RELEASE,
    BTN_PRESS,
    BTN_HELD
} ACTION_TYPE;
typedef enum
{
    DPAD_UP,
    DPAD_DOWN,
    DPAD_LEFT,
    DPAD_RIGHT,
    DPAD_UP_HELD,
    DPAD_DOWN_HELD,
    DPAD_LEFT_HELD,
    DPAD_RIGHT_HELD
} DPAD_DIRECTION;

typedef uint8_t dpad_t;

typedef struct inputs
{
    uint8_t btn_a;
    uint8_t btn_b;
    uint8_t btn_x;
    uint8_t btn_y;
    uint8_t btn_start;
    uint8_t trg_left;
    uint8_t trg_right;
    dpad_t dpad;
} inputs;

void INPT_ReceiveFromHost(inputs _in);

bool INPT_Button(BUTTON btn);
bool INPT_ButtonEx(BUTTON btn, ACTION_TYPE type);
dpad_t INPT_DPAD();
bool INPT_DPADDirection(DPAD_DIRECTION dir);

#endif /* INPUT_H */
