#ifndef RENDERER_H
#define RENDERER_H
/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\common\renderer.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\common
 * Created Date: Friday, June 28th 2019, 8:34:48 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */
#include "common.h"

#ifdef _arch_dreamcast
#include "gl.h"
#include "glu.h"
#include "glkos.h"
#include "glext.h"
#endif
#ifdef __MINGW32__ 
#define GLFW_INCLUDE_GLU
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#endif
#ifdef PSP
#include <GL/gl.h>	// Header File For The OpenGL32 Library
#include <GL/glu.h>	// Header File For The GLu32 Library
#include <GLES/egl.h>
#undef psp_log
extern void __pspgl_log (const char *fmt, ...);
/* disable verbose logging to "ms0:/log.txt" */
#if 1
	#define psp_log(x...) __pspgl_log(x)
#else
	#define psp_log(x...) do {} while (0)
#endif

/* enable EGLerror logging to "ms0:/log.txt" */
#if 1
	#define EGLCHK(x)							\
	do {									\
		EGLint errcode;							\
		x;								\
		errcode = eglGetError();					\
		if (errcode != EGL_SUCCESS) {					\
			__pspgl_log("%s (%d): EGL error 0x%04x\n",			\
				__FUNCTION__, __LINE__,				\
				(unsigned int) errcode);			\
		}								\
	} while (0)
#else
	#define EGLCHK(x) x
#endif
#endif

#include "image_loader.h"

typedef struct image image;

#define glCheckError() glCheckError_(__FILE__, __LINE__) 
GLenum glCheckError_(const char *file, int line);

// We call this right after our OpenGL window is created.
void InitGL(int Width, int Height);	        

/* The function called when our window is resized (which shouldn't happen, because we're fullscreen) */
void ReSizeGLScene(int Width, int Height);

GLuint RNDR_CreateTextureFromImage(image *img);

inline void GL_Bind(image img){
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, img.id);
}

// settings
extern const unsigned int SCR_WIDTH;
extern const unsigned int SCR_HEIGHT;

#endif /* RENDERER_H */
